import Vue from "vue";
import Vuex from "vuex";
import ToornamentService from "@/services/ToornamentService";

Vue.use(Vuex);

const state = {
    stageRanking: [],
    stageRankingHeaders: [
        { text: "#", value: "moot", sortable: false },
        { text: "Rank", value: "rank", sortable: false },
        { text: "Player", value: "name", sortable: false },
        { text: "Match wins", value: "matchWins" },
        { text: "Match losses", value: "matchLosses" },
        { text: "Match diff.", value: "matchDiff" },
        { text: "Map wins", value: "mapWins" },
        { text: "Map losses", value: "mapLosses" },
        { text: "Map diff.", value: "mapDiff" },
        { text: "Prizing", value: "points" }
    ],
    identifiers: {
        tournamentId: "6400653364376731648",
        stageId: "6400665280854941696"
    },
    loading: true
};

const mutations = {
    SET_STAGE_RANKING(state, stageRanking) {
        state.stageRanking = stageRanking;
    },
    SET_LOADING(state, bool) {
        state.loading = bool;
    }
};

const actions = {
    fetchStageRanking({ commit, state }) {
        commit("SET_LOADING", true);
        const options = state.identifiers;
        ToornamentService.getRanking(options).then(response => {
            const stageRanking = response.data.map(item => {
                return {
                    moot: null,
                    rank: item.rank,
                    name: item.participant.name,
                    matchWins: item.properties.wins,
                    matchLosses: item.properties.losses,
                    matchDiff: item.properties.wins - item.properties.losses,
                    mapWins: item.properties.score_for,
                    mapLosses: item.properties.score_against,
                    mapDiff:
                        item.properties.score_for -
                        item.properties.score_against,
                    points: item.points
                };
            });
            commit("SET_STAGE_RANKING", stageRanking);
            commit("SET_LOADING", false);
        });
    }
};

const getters = {
    orderedStageRanking: state => {
        return state.stageRanking.sort((a, b) => {
            return a.rank - b.rank;
        });
    }
};

export default new Vuex.Store({
    state,
    mutations,
    actions,
    modules: {},
    getters
});
