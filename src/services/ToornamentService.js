import axios from "axios";

const apiClient = axios.create({
    baseURL: "https://api.toornament.com/viewer/v2",
    // api-key taken from https://quake.pglesports.com/
    headers: {
        "X-Api-Key": "KXBVgSRLKrw-7-FTBBH8YpripaMDbbmPrg-pD1TqWoQ",
        Range: "items=0-49"
    }
});

export default {
    getRanking({ tournamentId, stageId }) {
        return apiClient.get(
            `/tournaments/${tournamentId}/stages/${stageId}/ranking-items`
        );
    }
};
