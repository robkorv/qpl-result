module.exports = {
    publicPath: process.env.NODE_ENV === "production" ? "/qpl-result/" : "/",
    transpileDependencies: ["vuetify"],
    productionSourceMap: false
};
